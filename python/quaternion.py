
from math import sqrt

# base routines for all numeric type

def conj(val):
    if isinstance(val, int):
        return val
    elif isinstance(val, float):
        return val
    elif isinstance(val, complex):
        return val.conjugate()
    elif isinstance(val, Quaternion):
        return val.conjugate()
    raise TypeError("conj does not accept: {!r}".format(val))

def norm(val):
    if isinstance(val, int):
        return abs(val)
    elif isinstance(val, float):
        return abs(val)
    elif isinstance(val, complex):
        return abs(val)
    elif isinstance(val, Quaternion):
        return val.norm()
    raise TypeError("norm does not accept: {!r}".format(val))


class Quaternion:
    def __init__(self, *args):
        if len(args) == 0:
            val = (0.0, 0.0, 0.0, 0.0) # default
        elif len(args) == 1:
            val = args[0]
        elif len(args) == 4:
            val = tuple(args)
        else:
            raise ValueError("Invalid initializer for Quaternion : {!r}".format(args))

        if isinstance(val, Quaternion):
            t = list(val.components)
        elif isinstance(val, int):
            t = [float(val), 0.0, 0.0, 0.0]
        elif isinstance(val, float):
            t = [val, 0.0, 0.0, 0.0]
        elif isinstance(val, complex):
            t = [val.real, val.imag, 0.0, 0.0]
        elif isinstance(val, list) or isinstance(val, tuple):
            # if we later support other internal types, add to list here
            if len(val) != 4 or not all(type(x) in [int,float] for x in val):
                raise TypeError("Invalid initializer for Quaternion : {!r}".format(val))
            # convert ints to float, let other types pass
            t = list((float(x) if isinstance(x,int) else x) for x in val)
        else:
            raise TypeError("Invalid initializer for Quaternion : {!r}".format(val))

        self.components = t

    @staticmethod
    def _cast(val,errmsg="Value is not interprettable as a Quaternion"):
        if isinstance(val, Quaternion):
            return val
        try:
            return Quaternion(val)
        except:
            pass
        raise TypeError("{} : {!r}".format(errmsg,val))


    def conjugate(self):
        a0,a1,a2,a3 = self.components
        return Quaternion(a0,-a1,-a2,-a3)

    def conj(self):
        return self.conjugate()

    def real(self):
        return self.components[0]

    def imag(self):
        q = Quaternion(self)
        q.components[0] = 0.0
        return q

    def norm2(self):
        return self.__mul__(self.conjugate()).real()
    
    def norm(self):
        return sqrt(self.norm2())

    @staticmethod
    def approx_equal(x,y):
        return sum(abs(d) for d in (x-y).components) < 0.0000001
    
    def __eq__(self, other):
        if isinstance(other,Quaternion):
            q = other
        else:
            try:
                q = Quaternion(other)
            except:
                return false
        return self.components == q.components

    
    def __neg__(self):
        q = Quaternion(self)
        q.components = list(-x for x in q.components)
        return q

    def __pos__(self):
        q = Quaternion(self)
        return q

    def mult_inv(self):
        q = self.conjugate()
        n2 = self.norm2()
        q.components = list(x/n2 for x in q.components)
        return q

    # Note: might want to consider __coerce__ instead of explict _cast

    def __add__(self, other):
        q = Quaternion._cast(other, "Cannot add Quaternion with")
        a0,a1,a2,a3 = self.components
        b0,b1,b2,b3 = q.components
        return Quaternion(a0+b0, a1+b1, a2+b2, a3+b3)

    def __sub__(self, other):
        q = Quaternion._cast(other, "Cannot sub Quaternion with")
        a0,a1,a2,a3 = self.components
        b0,b1,b2,b3 = q.components
        return Quaternion(a0-b0, a1-b1, a2-b2, a3-b3)

    def __mul__(self, other):
        q = Quaternion._cast(other, "Cannot multiply Quaternion with")
        a0,a1,a2,a3 = self.components
        b0,b1,b2,b3 = q.components
        c0 = a0*b0 - a1*b1 - a2*b2 - a3*b3
        c1 = a0*b1 + a1*b0 + a2*b3 - a3*b2
        c2 = a0*b2 + a2*b0 - a1*b3 + a3*b1
        c3 = a0*b3 + a3*b0 + a1*b2 - a2*b1
        return Quaternion(c0,c1,c2,c3)

    def __truediv__(self, other):
        q = Quaternion._cast(other, "Cannot divide Quaternion with")
        return self * q.mult_inv()
    
    # python2
    def __div__(self, other):
        q = Quaternion._cast(other, "Cannot divide Quaternion with")
        return self * q.mult_inv()


    def __radd__(self, other):
        q = Quaternion._cast(other, "Cannot radd Quaternion with")
        return q + self

    def __rsub__(self, other):
        q = Quaternion._cast(other, "Cannot rsub Quaternion with")
        return q - self

    def __rmul__(self, other):
        q = Quaternion._cast(other, "Cannot rmultiply Quaternion with")
        return q * self

    def __rtruediv__(self, other):
        q = Quaternion._cast(other, "Cannot rdivide Quaternion with")
        return q / self

    # python2
    def __rdiv__(self, other):
        q = Quaternion._cast(other, "Cannot rdivide Quaternion with")
        return q / self


    def __repr__(self):
        return "Quaternion{!r}".format(tuple(self.components))


if __name__ == "__main__":
    # do some tests

    # Various constructors
    q = Quaternion()
    assert(q.components == [0.0, 0.0, 0.0, 0.0])
    q = Quaternion(11)
    assert(q.components == [11.0, 0.0, 0.0, 0.0])
    q = Quaternion(1.1)
    assert(q.components == [1.1, 0.0, 0.0, 0.0])
    q = Quaternion(1.1 + 2.2j)
    assert(q.components == [1.1, 2.2, 0.0, 0.0])
    q = Quaternion(1,2,3,4)
    assert(q.components == [1.0, 2.0, 3.0, 4.0])
    q2 = Quaternion(q)
    q.components[1] = 4 # change q, should not affect q2
    assert(q2.components == [1.0, 2.0, 3.0, 4.0])
    q = Quaternion([5,6,7.0,8])
    assert(q.components == [5.0, 6.0, 7.0, 8.0])
    q = Quaternion((5,6,7.0,8))
    assert(q.components == [5.0, 6.0, 7.0, 8.0])

    # equality
    q = Quaternion(1,2,3,4)
    q2 = Quaternion(1,2,3,4)
    assert(q == q2)
    q2 = Quaternion(1)
    assert(q != q2)
    assert(q2 == 1)
    assert(1 == q2)
    assert(q2 == [1,0,0,0])

    # unary operations and properties
    q = Quaternion(4.0, 2.0, 2.0, 1.0)
    assert(repr(q) == "Quaternion(4.0, 2.0, 2.0, 1.0)")
    assert(q.conjugate() == [4.0, -2.0, -2.0, -1.0])
    assert(q.real() == 4.0)
    assert(q.imag() == [0.0, 2.0, 2.0, 1.0])
    assert(q.norm2() == 25.0)
    assert(q.norm() == 5.0)
    assert( -q == [-4.0, -2.0, -2.0, -1.0])
    assert( +q == [4.0, 2.0, 2.0, 1.0])
    assert(q.mult_inv() == [0.16, -0.08, -0.08, -0.04])

    # arithmetic
    q = Quaternion(4.0, 2.0, 2.0, 1.0)
    q2 = Quaternion(9.0, 3.0, 3.0, 1.0)

    assert(q + q2 == Quaternion(13.0, 5.0, 5.0, 2.0))
    assert(2.5 + q == Quaternion(6.5, 2.0, 2.0, 1.0))
    assert(q + 2.5 == Quaternion(6.5, 2.0, 2.0, 1.0))

    assert(q - q2 == Quaternion(-5.0, -1.0, -1.0, 0.0))
    assert(2.5 - q == Quaternion(-1.5, -2.0, -2.0, -1.0))
    assert(q - 2.5 == Quaternion(1.5, 2.0, 2.0, 1.0))

    assert(q * q2 == Quaternion(23.0, 29.0, 31.0, 13.0))
    assert(q2 * q == Quaternion(23.0, 31.0, 29.0, 13.0))
    assert(2.5 * q == Quaternion(10.0, 5.0, 5.0, 2.5))
    assert(q * 2.5 == Quaternion(10.0, 5.0, 5.0, 2.5))

    q = Quaternion(2, 3, 5, 7)
    q2 = Quaternion(11, 13, 17, 19)

    assert(Quaternion.approx_equal(q.mult_inv(), [2.0/87, -3.0/87, -5.0/87, -7.0/87]))
    assert(Quaternion.approx_equal(q2.mult_inv(), [11.0/940, -13.0/940, -17.0/940, -19.0/940]))

    assert(Quaternion.approx_equal(q / q2, [279.0/940, 31.0/940, -13.0/940, 53.0/940]))
    assert(Quaternion.approx_equal(q2 / q, [279.0/87, -31.0/87, 13.0/87, -53.0/87]))
    assert(Quaternion.approx_equal(2.5 / q, [5.0/87, -5.0/58, -25.0/174, -35.0/174]))
    assert(Quaternion.approx_equal(q / 2.5, [4.0/5, 6.0/5, 2.0, 14.0/5]))

    print("All tests passed")

