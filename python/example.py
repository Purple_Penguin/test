
from quaternion import *
from math import sqrt, cos, sin, pi

k = sqrt(2)
h = Quaternion(0,1/k,1/k,0)
B = Quaternion(0,1,0,0)
B2 = h * B * conj(h) + (1/2)*( conj(h*h*B) - conj(conj(h)*conj(h)*B) )

print(B2)


def Rot(theta, q_axis):
    """
    Given an angle (radians),
      and a purely imaginary quaternion to denote a spatial direction, this
    returns a function that rotates quaternions by than angle around that axis
    """
    if q_axis.real() != 0:
        raise Exception(ValueError)
    # normalize the quaternion defining the spatial axis
    a = q_axis / q_axis.norm()
    v = cos(theta/2.0) + sin(theta/2.0) * a
    return lambda q : v * q * v.conj()

Rz90deg = Rot(pi/2.0, Quaternion(0,0,0,1))

print(Rz90deg(B))


