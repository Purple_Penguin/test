\documentclass[11pt,letterpaper]{article}

%% simple/quick difference between common doc classes
% https://texblog.org/2007/07/09/documentclassbook-report-article-or-letter/

%% pulled examples from
% https://www.overleaf.com/learn/latex/Hyperlinks}{hyperref package
% https://www.overleaf.com/latex/templates/chapter-review-notes/npqqbrvfkwqh

\usepackage{amsmath}
\usepackage{amsthm}

%% not sure what this does \theoremstyle{definition}
\newtheorem{defn}{Definition}
\newtheorem{reg}{Rule}
\newtheorem{exer}{Exercise}
\newtheorem{note}{Note}
\newtheorem{thm}{Theorem}

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}

\usepackage{framed}  % for \begin{shaded} color box \end{shaded}
\usepackage{xcolor}
\colorlet{shadecolor}{orange!15}

%% if you prefer square paragraphs
\usepackage{parskip}
%\usepackage{parfill} % I don't have, but recommended in comments: https://tex.stackexchange.com/questions/40429/how-to-use-the-parskip-package-space-in-between-paragraphs

%% can try adjusting by hand, but packages are better
%\setlength{\parindent}{0.0in}
%\setlength{\parskip}{0.05in}
%\parindent 0in
%\parskip 6pt
%\geometry{margin=1in, headsep=0.25in}

%\usepackage{placeins} % for \FloatBarrier
% but didn't work like I expected

\usepackage[colorinlistoftodos,color=green!20,linecolor=gray]{todonotes}
% ... disable display of todonotes without needing to remove them
%\usepackage[disable]{todonotes}
\newcommand{\todored}[2][]
{\todo[color=red!20, #1]{#2}}
\newcommand{\todoline}[2][]
{\todo[inline, #1]{#2}}

\usepackage{hyperref}
\hypersetup{
	colorlinks=true,
	linkcolor=blue,
	filecolor=magenta,      
	urlcolor=blue, %cyan violet
}

% block indenting
% https://tex.stackexchange.com/questions/37080/how-can-i-indent-a-block-of-text-for-a-specified-amount
\usepackage{scrextend}
\newcommand{\blockindent}[2][1em]
{
\begin{addmargin}[#1]{0em}% 1em left, 0em right
  #2
\end{addmargin}
}

% default placement is tbp but bottom floats will go past footnotes, which I really don't like
% not sure how to get this to work
%\renewcommand{\fps@figure}{htp}
%\renewcommand{\fps@table}{htp}


\title{The Quadratic Formula}

\author{Amanda King}

\date{\today}
\begin{document}
\maketitle

\begin{abstract}
This paper will state and prove the quadratic formula.
\end{abstract}

\tableofcontents
\pagebreak

\section{Introduction}

We have all, at some point, learned the quadratic formula. Usually, we learn how to use it to find solutions of a quadratic equation. We might have seen the proof at this point, but most of us did not commit it to memory since the proof does not help us to use the formula.

\section{The Quadratic Formula}
\label{sec:examples}

\begin{thm} 
\label{quad-theorem}	
Let a, b, and c be real number. The solutions of $ax^2+bx+c=0$ are 
$$x=\frac{-b\pm \sqrt{b^2-4ac}}{2a}.$$
\end{thm}

\begin{proof} In order to prove the quadratic formula, we use the process of completing the square. Starting with $ax^2+bx+c=c$, arithmetic gives 
\begin{align*} 
\\ax^2+bx&=-c,
\\x^2+\frac{b}{a}x&=-\frac{c}{a},
\\x^2+\frac{b}{a}x+\frac{b^2}{4a^2}&=-\frac{c}{a}+\frac{b^2}{4a^2},
\\\Bigg(x+\frac{b}{2a}\Bigg)^2&=-\frac{4ac}{4a^2}+\frac{b^2}{4a^2},
\\x+\frac{b}{2a}&=\pm\sqrt[]{\frac{b-4ac}{4a^2}}
\\x&=\frac{-b\pm\sqrt[]{b^2-4ac}}{2a}
\end{align*}
\end{proof}

Later we will use theorem \ref{quad-theorem} to solve problems.

\section{Examples}

% this is a comment

This was originally taken from overleaf.com\footnote{
\url{https://www.overleaf.com/articles/the-quadratic-formula/wprdthgttvzm} }
which was an example with the tags: handout, math, lecture notes.

The \href{https://www.overleaf.com/learn/latex/Hyperlinks}{hyperref package} has some nice macros.

Comments can be added to the margins of the document using the \todo{Here's a comment in the margin!} todo command, as shown in the example on the right. You can also add inline comments:

\todo[inline]{This is an inline comment.}

Some tests of my macros
\todored[inline]{This is a red comment.}
\todoline{todoline gives an inline comment.}

\emph{case 1}:
\blockindent{
  The quick brown fox jumped over the lazy dog. The cow jumped over the moon, and the fork ran away with the spoon. Who knew? Not me, not you.
  
  What the what?

  \blockindent{
    further indent
  }
  End of case 1
}


Back to normal
\begin{addmargin}[1em]{0em}
  Here there be dragons
\end{addmargin}
After dragons.

Here are some important notes
\begin{shaded}
\begin{note}
This is an elliptic curve
$$ y^2 = 4x^3 - 17 $$
\end{note}
\end{shaded}


\subsection{How to Include Figures}

First you have to upload the image file (JPEG, PNG or PDF) from your computer to writeLaTeX using the upload link the project menu. Then use the includegraphics command to include it in your document. Use the figure environment and the caption command to add a number and a caption to your figure. See the code for Figure \ref{fig:frog} in this section for an example.

% https://en.wikibooks.org/wiki/LaTeX/Floats,_Figures_and_Captions

\begin{figure}[h]  % h = 'here' placement
\centering
\includegraphics[width=0.3\textwidth]{frog.jpg}
\caption{\label{fig:frog}This frog was uploaded to writeLaTeX via the project menu.}
\end{figure}


\subsection{How to Make Tables}

Use the table and tabular commands for basic tables --- see Table~\ref{tab:widgets}, for example.

\begin{table}
\centering
\begin{tabular}{l|r}
Item & Quantity \\\hline
Widgets & 42 \\
Gadgets & 13
\end{tabular}
\caption{\label{tab:widgets}An example table.}
\end{table}

\subsection{How to Write Mathematics}

\LaTeX{} is great at typesetting mathematics. Let $X_1, X_2, \ldots, X_n$ be a sequence of independent and identically distributed random variables with $\text{E}[X_i] = \mu$ and $\text{Var}[X_i] = \sigma^2 < \infty$, and let
$$S_n = \frac{X_1 + X_2 + \cdots + X_n}{n}
      = \frac{1}{n}\sum_{i}^{n} X_i$$
denote their mean. Then as $n$ approaches infinity, the random variables $\sqrt{n}(S_n - \mu)$ converge in distribution to a normal $\mathcal{N}(0, \sigma^2)$.

\subsection{How to Make Sections and Subsections}

Use section and subsection commands to organize your document. \LaTeX{} handles all the formatting and numbering automatically. Use ref and label commands for cross-references.

\subsection{How to Make Lists}

You can make lists with automatic numbering \dots

\begin{enumerate}
\item Like this,
\item and like this.
\end{enumerate}
\dots or bullet points \dots
\begin{itemize}
\item Like this,
\item and like this.
\end{itemize}
\dots or with words and descriptions \dots
\begin{description}
\item[Word] Definition
\item[Concept] Explanation
\item[Idea] Text
\end{description}

We hope you find write\LaTeX\ useful, and please let us know if you have any feedback using the help menu above.

\section{Future work}
\listoftodos
% optionally give it a name \listoftodos[List of future work]

\end{document}